package kz.bcc.report.jasperreportservice.service;

import kz.bcc.report.jasperreportservice.error.ReportServiceException;
import kz.bcc.report.jasperreportservice.parametr.ReportParam;
import kz.bcc.report.jasperreportservice.parametr.ReportParamV2;
import kz.bcc.report.jasperreportservice.parametr.ReportType;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JsonQLDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXmlExporter;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static kz.bcc.report.jasperreportservice.error.ReportServiceExceptionEnum.GENERATE_ERROR;
import static kz.bcc.report.jasperreportservice.error.ReportServiceExceptionEnum.UNKNOWN_TYPE;


@Service
public class GeneratorServiceImpl implements GeneratorService {

    private final static String ENCODE = "UTF8";


    public ByteArrayOutputStream generateWithSources(String template, String data, Map<String, Object> parameters, ReportType type) {
        try {
            InputStream templateStream = new ByteArrayInputStream(template.getBytes(StandardCharsets.UTF_8));
            InputStream dataStream = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));


            JasperDesign jasperDesign = JRXmlLoader.load(templateStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

            JRDataSource source = new JsonQLDataSource(dataStream);

            return createContent(type, jasperReport, parameters, source);
        }catch (Exception e){
            throw new ReportServiceException(GENERATE_ERROR, "Error occurred on generate document", e);
        }
    }

    private Exporter getExporter(ReportType type){
        switch (type){
            case ODS: return new JROdsExporter();
            case PDF: return new JRPdfExporter();
            case ODT: return new JROdtExporter();
            case XLS: return new JRXlsExporter();
            case XML: return new JRXmlExporter();
            case DOCX: return new JRDocxExporter();
            case XLSX: return new JRXlsxExporter();


            default: throw new ReportServiceException(UNKNOWN_TYPE, "Can't generate document for type " + type);
        }
    }

    @Override
    public ByteArrayOutputStream generateWithParams(String template, Map<String, Object> parameters, ReportType type) {

        try {
            InputStream templateStream = new ByteArrayInputStream(template.getBytes(StandardCharsets.UTF_8));
            JasperDesign jasperDesign = JRXmlLoader.load(templateStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

            JRDataSource source = new JREmptyDataSource();

            return createContent(type, jasperReport, parameters, source);
        }catch (Exception e){
            throw new ReportServiceException(GENERATE_ERROR, "Error occurred on generate document", e);
        }
    }

    private ByteArrayOutputStream createContent(ReportType type, JasperReport jasperReport, Map<String, Object> parameters, JRDataSource source) throws JRException, IOException {

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, source);
        Exporter exporter = getExporter(type);
        try(ByteArrayOutputStream content = new ByteArrayOutputStream()) {

            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(content));
            exporter.exportReport();

            return content;
        }

    }

    @Override
    public ByteArrayOutputStream generate(ReportParam param) throws UnsupportedEncodingException {

        String template = URLDecoder.decode(param.getTemplate(), ENCODE);
        String data = URLDecoder.decode(param.getData(), ENCODE);

        return generateWithSources(template, data, param.getParams(), param.getType());
    }

    @Override
    public ByteArrayOutputStream generate(ReportParamV2 param) throws UnsupportedEncodingException {
        return generateWithParams(URLDecoder.decode(param.getTemplate(), ENCODE), param.getData(), param.getType());
    }
}
