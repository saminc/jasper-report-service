package kz.bcc.report.jasperreportservice.service;

import kz.bcc.report.jasperreportservice.parametr.ReportParam;
import kz.bcc.report.jasperreportservice.parametr.ReportParamV2;
import kz.bcc.report.jasperreportservice.parametr.ReportType;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface GeneratorService {

    ByteArrayOutputStream generateWithSources(String template, String data, Map<String, Object> parameters, ReportType type);

    ByteArrayOutputStream generate(ReportParam param) throws UnsupportedEncodingException;

    ByteArrayOutputStream generate(ReportParamV2 param) throws UnsupportedEncodingException;

    ByteArrayOutputStream generateWithParams(String template, Map<String, Object> parameters, ReportType type);
}
