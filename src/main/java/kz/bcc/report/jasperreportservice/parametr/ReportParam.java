package kz.bcc.report.jasperreportservice.parametr;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ReportParam {

    private String template;

    private String data = "{}";

    private ReportType type;

    private Map<String, Object> params = new HashMap<>();

    public ReportParam() {
    }
}
