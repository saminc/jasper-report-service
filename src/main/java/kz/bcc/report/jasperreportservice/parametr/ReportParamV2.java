package kz.bcc.report.jasperreportservice.parametr;

import lombok.Data;

import java.util.Map;

@Data
public class ReportParamV2 {

    private String template;

    private Map<String, Object> data;

    private ReportType type;
}
