package kz.bcc.report.jasperreportservice.parametr;

public enum ReportType {

    PDF,
    XLS,
    XML,
    ODS,
    ODT,
    DOCX,
    XLSX
}





