package kz.bcc.report.jasperreportservice.controller;

import kz.bcc.report.jasperreportservice.error.ReportServiceException;
import kz.bcc.report.jasperreportservice.error.ReportServiceExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
public class ErrorControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ReportServiceException.class})
    public ResponseEntity<Object> generateDocumentError(ReportServiceException ex, final WebRequest request) {
        log.error("Error occurred on generate document", ex);
        val msg = (ex.getCause() == null
                ? ex.getMessage()
                : ex.getMessage() + ": " + ex.getCause().getMessage());
        val headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        val body = new ErrorResponse(ex.getType(), msg);
        return handleExceptionInternal(ex, body, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class})
    public ResponseEntity<Object> handleInternal(final RuntimeException ex, final WebRequest request) {
        logger.error("500 Status Code", ex);
        val headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        val body = new ErrorResponse(ReportServiceExceptionEnum.UNKNOWN_TYPE,
                "This should be application specific: " + ex.getMessage());
        return handleExceptionInternal(ex, body, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @AllArgsConstructor
    @Getter
    private static class ErrorResponse {
        private final ReportServiceExceptionEnum errorType;
        private final String errorReason;
    }
}
