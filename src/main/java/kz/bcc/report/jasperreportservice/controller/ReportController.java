package kz.bcc.report.jasperreportservice.controller;

import kz.bcc.report.jasperreportservice.parametr.ReportParam;
import kz.bcc.report.jasperreportservice.parametr.ReportParamV2;
import kz.bcc.report.jasperreportservice.service.GeneratorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ReportController {

    private final GeneratorService reportService;

    @RequestMapping(value = "/report", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
    public void createReport(@RequestBody ReportParam param, HttpServletResponse response) throws IOException {
        log.debug(param.toString());
        reportService.generate(param).writeTo(response.getOutputStream());
    }

    @PostMapping(value = "/api/v/2/report", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createReportV2(@RequestBody ReportParamV2 param, HttpServletResponse response) throws IOException {
        reportService.generate(param).writeTo(response.getOutputStream());
        return ResponseEntity.ok().build();
    }


    @GetMapping(value = "/info")
    public String info(){
        return "ok";
    }
}
