package kz.bcc.report.jasperreportservice.error;

public enum  ReportServiceExceptionEnum {

    OTHER_ERROR,
    GENERATE_ERROR,
    UNKNOWN_TYPE,
    CREATE_FILE_ERROR,
    FILE_NOT_FOUND;
}
