package kz.bcc.report.jasperreportservice.error;

public class ReportServiceException extends RuntimeException {

    private final ReportServiceExceptionEnum type;



    public ReportServiceException(ReportServiceExceptionEnum type, String message) {
        super(message);
        this.type = type;
    }

    public ReportServiceException(ReportServiceExceptionEnum type, String message, Throwable cause) {
        super(message, cause);
        this.type = type;
    }

    public ReportServiceExceptionEnum getType() {
        return type;
    }
}
